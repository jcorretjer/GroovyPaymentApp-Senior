package com.imobile3.groovypayments.ui.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.imobile3.groovypayments.R;
import com.imobile3.groovypayments.data.enums.GroovyColor;
import com.imobile3.groovypayments.data.enums.GroovyIcon;
import com.imobile3.groovypayments.data.model.Product;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ProductListAdapter
        extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {

    private Context mContext;
    private AdapterCallback mCallbacks;
    private List<Product> mItems;

    public interface AdapterCallback {

        void onProductClick(Product product);
    }

    public ProductListAdapter(
            @NonNull Context context,
            @NonNull List<Product> products,
            @NonNull AdapterCallback callback) {
        mContext = context;
        mCallbacks = callback;
        mItems = products;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.product_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Product item = mItems.get(position);

        holder.label.setText(item.getName());

        holder.desc.setText
                (
                        new StringBuilder().append(item.getUnitPrice())
                                           .append("|")
                                           .append(item.getNote())
                );

        holder.productImgVw.setImageResource(GroovyIcon.fromId(item.getIconId()).drawableRes);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            holder.backgroundVW.setBackgroundColor(mContext.getColor(GroovyColor.fromId(item.getColorId()).colorRes));

        else
            holder.backgroundVW.setBackgroundColor(ResourcesCompat.getColor(mContext.getResources(), GroovyColor.fromId(item.getColorId()).colorRes, null));

        /*holder.label.setTextColor(
                StateListHelper.getTextColorSelector(mContext, R.color.black_space));*/
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ViewGroup container;

        TextView label,
        desc;

        View backgroundVW;

        ImageView productImgVw;

        ViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            label = itemView.findViewById(R.id.productTitleTxtVw);
            container.setOnClickListener(this);

            desc = itemView.findViewById(R.id.productItemDescTxtVw);

            productImgVw = itemView.findViewById(R.id.productImgVw);

            backgroundVW = itemView.findViewById(R.id.productBackgroundVw);
        }

        @Override
        public void onClick(View v) {
            if (v == container) {
                mCallbacks.onProductClick(mItems.get(getAdapterPosition()));
            }
        }
    }

    public List<Product> getItems() {
        return mItems;
    }

    public void setItems(@Nullable List<Product> items) {
        mItems = items != null ? items : new ArrayList<>();
        notifyDataSetChanged();
    }
}
