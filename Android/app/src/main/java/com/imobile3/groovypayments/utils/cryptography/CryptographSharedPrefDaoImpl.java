package com.imobile3.groovypayments.utils.cryptography;

import android.content.SharedPreferences;

public class CryptographSharedPrefDaoImpl extends CryptographSharedPrefDao
{
    protected CryptographSharedPrefDaoImpl(SharedPreferences sharedPreferences)
    {
        super(sharedPreferences);
    }

    @Override
    public void InsertEncodedKey(String key, String val)
    {
        sharedPreferences.edit().putString(key, val).apply();
    }

    @Override
    public String GetEncodedKey(String key, String defaultVal)
    {
        return sharedPreferences.getString(key, defaultVal);
    }

    private static CryptographSharedPrefDaoImpl instance;

    public static synchronized CryptographSharedPrefDaoImpl getInstance(SharedPreferences sharedPref)
    {
        if(instance == null)
            instance = new CryptographSharedPrefDaoImpl(sharedPref);

        return instance;
    }
}
