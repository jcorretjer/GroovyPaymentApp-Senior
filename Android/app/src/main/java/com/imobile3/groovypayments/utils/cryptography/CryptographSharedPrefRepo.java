package com.imobile3.groovypayments.utils.cryptography;

import android.content.SharedPreferences;

public class CryptographSharedPrefRepo
{
    private CryptographSharedPrefDaoImpl dao;

    private static CryptographSharedPrefRepo instance;

    public CryptographSharedPrefRepo(CryptographSharedPrefDaoImpl cryptographSharedPrefDao)
    {
        this.dao = cryptographSharedPrefDao;
    }

    public void InsertEncodedKey(String key, String val)
    {
        dao.InsertEncodedKey(key, val);
    }

    public String GetEncodedKey(String key, String defaultVal)
    {
        return dao.GetEncodedKey(key, defaultVal);
    }

    public static synchronized CryptographSharedPrefRepo getInstance(SharedPreferences sharedPreferences)
    {
        if(instance == null)
            instance = new CryptographSharedPrefRepo(CryptographSharedPrefDaoImpl.getInstance(sharedPreferences));

        return instance;
    }
}
