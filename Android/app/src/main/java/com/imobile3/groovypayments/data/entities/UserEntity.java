package com.imobile3.groovypayments.data.entities;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "user")
public class UserEntity implements Parcelable
{

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "user_id")
    private long mId;

    @ColumnInfo(name = "first_name")
    private String mFirstName;

    @ColumnInfo(name = "last_name")
    private String mLastName;

    @ColumnInfo(name = "username")
    private String mUsername;

    @ColumnInfo(name = "email")
    private String mEmail;

    @ColumnInfo(name = "password")
    private String mPassword;

    @ColumnInfo(name = "passwordIV")
    private String mPasswordIV;

    public String getPasswordIV()
    {
        return mPasswordIV;
    }

    public void setPasswordIV(String mPasswordIV)
    {
        this.mPasswordIV = mPasswordIV;
    }

    public UserEntity() {
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeLong(this.mId);
        dest.writeString(this.mFirstName);
        dest.writeString(this.mLastName);
        dest.writeString(this.mUsername);
        dest.writeString(this.mEmail);
        dest.writeString(this.mPassword);
        dest.writeString(this.mPasswordIV);
    }

    public void readFromParcel(Parcel source)
    {
        this.mId = source.readLong();
        this.mFirstName = source.readString();
        this.mLastName = source.readString();
        this.mUsername = source.readString();
        this.mEmail = source.readString();
        this.mPassword = source.readString();
        this.mPasswordIV = source.readString();
    }

    protected UserEntity(Parcel in)
    {
        this.mId = in.readLong();
        this.mFirstName = in.readString();
        this.mLastName = in.readString();
        this.mUsername = in.readString();
        this.mEmail = in.readString();
        this.mPassword = in.readString();
        this.mPasswordIV = in.readString();
    }

    public static final Parcelable.Creator<UserEntity> CREATOR = new Parcelable.Creator<UserEntity>()
    {
        @Override
        public UserEntity createFromParcel(Parcel source)
        {
            return new UserEntity(source);
        }

        @Override
        public UserEntity[] newArray(int size)
        {
            return new UserEntity[size];
        }
    };
}
