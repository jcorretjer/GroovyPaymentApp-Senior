package com.imobile3.groovypayments.data.utils;

import com.imobile3.groovypayments.data.entities.UserEntity;

public class User
{
    private String mFirstName;

    private String mLastName;

    private String mUsername;

    private String mEmail;

    private String mPassword;

    private UserEntity userEntity;

    private String mPasswordIV;

    public User Build(String email, String password, String passwordIV)
    {
        this.mEmail = email;

        this.mPassword = password;

        this.mPasswordIV = passwordIV;

        userEntity = new UserEntity();

        userEntity.setPassword(password);

        userEntity.setEmail(email);

        userEntity.setPasswordIV(passwordIV);

        return this;
    }

    public UserEntity BuildEntity()
    {
        return userEntity;
    }

    public String getFirstName()
    {
        return mFirstName;
    }

    public User setFirstName(String mFirstName)
    {
        this.mFirstName = mFirstName;

        userEntity.setFirstName(mFirstName);

        return this;
    }

    public String getLastName()
    {
        return mLastName;
    }

    public User setLastName(String mLastName)
    {
        this.mLastName = mLastName;

        userEntity.setLastName(mLastName);

        return this;
    }

    public String getUsername()
    {
        return mUsername;
    }

    public User setUsername(String mUsername)
    {
        this.mUsername = mUsername;

        userEntity.setUsername(mUsername);

        return this;
    }

    public String getEmail()
    {
        return mEmail;
    }

    public User setEmail(String mEmail)
    {
        this.mEmail = mEmail;

        userEntity.setEmail(mEmail);

        return this;
    }

    public String getPassword()
    {
        return mPassword;
    }

    public User setPassword(String mPassword)
    {
        this.mPassword = mPassword;

        userEntity.setPassword(mPassword);

        return this;
    }

    public String getPasswordIV()
    {
        return mPasswordIV;
    }

    public User setPasswordIV(String passwordIV)
    {
        this.mPasswordIV = passwordIV;

        userEntity.setPasswordIV(passwordIV);

        return this;
    }
}
