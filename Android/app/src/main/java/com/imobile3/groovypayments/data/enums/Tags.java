package com.imobile3.groovypayments.data.enums;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Tags
{
    @Retention(RetentionPolicy.SOURCE)
    @StringDef({SharedPref.SECURITY_PREF, SharedPref.SECURE_ENCODED_KEY_KEY})
    public @interface SharedPref
    {
        String SECURITY_PREF = "SECURITY_PREF",
                SECURE_ENCODED_KEY_KEY = "SECURE_ENCODED_KEY_KEY";
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({Bundle.USER_PROFILE_KEY})
    public @interface Bundle
    {
        String USER_PROFILE_KEY = "USER_PROFILE_KEY";
    }
}
