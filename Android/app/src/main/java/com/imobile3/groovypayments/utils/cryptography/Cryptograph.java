package com.imobile3.groovypayments.utils.cryptography;

import android.os.Build;
import android.util.Base64;

import androidx.annotation.IntDef;
import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class Cryptograph implements Krypto
{
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({KeyParts.ITERATION_COUNTS, KeyParts.LENGTH})
    public @interface KeyParts
    {
        int ITERATION_COUNTS = 65536,
                LENGTH = 256;
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({PasswordSeedlings.LOWER, PasswordSeedlings.UPPER, PasswordSeedlings.NUMBER})
    public @interface PasswordSeedlings
    {
        String LOWER = "abcdefghijklmnopqrstuvwxyz",
                UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                NUMBER = "0123456789";
    }

    private byte[] iv = new byte[16];

    private SecretKey secretKey;

    private String encodedKey;

    private static Cryptograph instance;

    public static synchronized void Init(String encodedKey) throws InvalidKeySpecException, NoSuchAlgorithmException
    {
        instance = new Cryptograph(encodedKey);
    }

    public static synchronized Cryptograph getInstance() throws InvalidKeySpecException, NoSuchAlgorithmException
    {
        return instance;
    }

    /**
     * The main encryption logic was taken from the official Google doc https://developer.android.com/guide/topics/security/cryptography
     * @param encodedKey
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    private Cryptograph(String encodedKey) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        //Generate new key
        if(encodedKey == null || encodedKey.isEmpty())
        {
            //region Generate salt

            byte[] salt = new byte[8];

            SecureRandom secureRandom = new SecureRandom();

            secureRandom.nextBytes(salt);

            //endregion

            //region Key password

            char[] passwdTxt = new char[13];

            final String PASSWORD_SEED = PasswordSeedlings.LOWER + PasswordSeedlings.UPPER + PasswordSeedlings.NUMBER;

            //Generate random password from seed
            for (int i = 0; i < passwdTxt.length; i++)
                passwdTxt[i] = PASSWORD_SEED.charAt(secureRandom.nextInt(PASSWORD_SEED.length()));

            //endregion

            //region Secret key

            final KeySpec KEY_SPEC = new PBEKeySpec(passwdTxt, salt, KeyParts.ITERATION_COUNTS, KeyParts.LENGTH);

            SecretKeyFactory secretKeyFactory;

            //Use a different encryption because PBKDF2WithHmacSHA256 doesn't exist on lower versions.
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
                secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2withHmacSHA1And8BIT");

            else
                secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");

            secretKey = new SecretKeySpec(secretKeyFactory.generateSecret(KEY_SPEC).getEncoded(), "AES");

            //endregion
        }

        //Generate the same secret key from provided encoded key
        else
        {
            final byte[] DECODED_KEY = Base64.decode(encodedKey, Base64.DEFAULT);

            secretKey = new SecretKeySpec(DECODED_KEY, 0, DECODED_KEY.length, "AES");
        }

        this.encodedKey = Base64.encodeToString(secretKey.getEncoded(), Base64.DEFAULT);

    }

    private static Cipher cipher;

    /**
     * Repeated invocations will use the same IV. To use a new IV, create a new instance of this object, for each new IV.
     * @return Encrypted message
     */
    @Override
    public byte[] Encrypt(String text) throws NoSuchPaddingException,
            NoSuchAlgorithmException,
            InvalidKeyException,
            InvalidParameterSpecException,
            BadPaddingException,
            IllegalBlockSizeException
    {
        if(cipher == null)
        {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            iv = cipher.getParameters()
                       .getParameterSpec(IvParameterSpec.class)
                       .getIV();
        }

        else
            iv = cipher.getParameters().getParameterSpec(IvParameterSpec.class).getIV();

        return cipher.doFinal(text.getBytes(StandardCharsets.UTF_8));
    }

    @Override
    public String Decrypt(byte[] encryptedText, byte[] iv) throws NoSuchPaddingException,
            NoSuchAlgorithmException,
            InvalidAlgorithmParameterException,
            InvalidKeyException,
            BadPaddingException,
            IllegalBlockSizeException
    {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

        cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv == null ? this.iv : iv));

        return new String(cipher.doFinal(encryptedText), StandardCharsets.UTF_8);
    }

    public byte[] GetIV()
    {
        return iv;
    }

    public String GetEncodedSecretKey()
    {
        return encodedKey;
    }
}
