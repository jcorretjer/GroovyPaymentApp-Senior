package com.imobile3.groovypayments.utils.cryptography;

import android.content.SharedPreferences;

public abstract class CryptographSharedPrefDao
{
    protected SharedPreferences sharedPreferences;

    public CryptographSharedPrefDao(SharedPreferences sharedPreferences)
    {
        this.sharedPreferences = sharedPreferences;
    }

    public abstract void InsertEncodedKey(String key, String val);

    public abstract String GetEncodedKey(String key, String defaultVal);
}
