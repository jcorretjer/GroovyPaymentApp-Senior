package com.imobile3.groovypayments.ui.main;

import androidx.lifecycle.ViewModel;

import com.imobile3.groovypayments.data.entities.UserEntity;

public class MainViewModel extends ViewModel
{
    private UserEntity loggedUser;

    public UserEntity getLoggedUser()
    {
        return loggedUser;
    }

    public void setLoggedUser(UserEntity loggedUser)
    {
        this.loggedUser = loggedUser;
    }

    // TODO: Finish implementing the ViewModel
}
