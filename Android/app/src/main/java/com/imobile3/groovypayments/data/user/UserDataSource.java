package com.imobile3.groovypayments.data.user;

import androidx.lifecycle.LiveData;

import com.imobile3.groovypayments.concurrent.GroovyExecutors;
import com.imobile3.groovypayments.data.DatabaseHelper;
import com.imobile3.groovypayments.data.entities.UserEntity;

import java.util.List;

public class UserDataSource
{
    public LiveData<UserEntity> getUserByEmail(String email)
    {
        return DatabaseHelper.getInstance().getDatabase().getUserDao().getUserByEmail(email);
    }

    public void InsertUsers(UserEntity... userEntities)
    {
        GroovyExecutors.getInstance().getDiskIo().execute(new Runnable()
        {
            @Override
            public void run()
            {
                DatabaseHelper.getInstance().getDatabase().getUserDao().insertUsers(userEntities);
            }
        });
    }

    public LiveData<List<UserEntity>> getAllUsers()
    {
        return DatabaseHelper.getInstance().getDatabase().getUserDao().getAllUsers();
    }
}
