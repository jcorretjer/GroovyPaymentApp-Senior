package com.imobile3.groovypayments.data.user;

import androidx.lifecycle.LiveData;

import com.imobile3.groovypayments.data.LoginDataSource;
import com.imobile3.groovypayments.data.LoginRepository;
import com.imobile3.groovypayments.data.entities.UserEntity;

import java.util.List;

public class UserRepo
{
    private static volatile UserRepo instance;

    private UserDataSource dataSource;

    private UserRepo(UserDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static UserRepo getInstance(UserDataSource dataSource)
    {
        if (instance == null)
            instance = new UserRepo(dataSource);

        return instance;
    }

    public LiveData<UserEntity> getUserByEmail(String email)
    {
        return dataSource.getUserByEmail(email);
    }

    public void InsertUsers(UserEntity... userEntities)
    {
        dataSource.InsertUsers(userEntities);
    }

    public LiveData<List<UserEntity>> getAllUser()
    {
        return dataSource.getAllUsers();
    }
}
