package com.imobile3.groovypayments.ui.login;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.imobile3.groovypayments.data.entities.UserEntity;
import com.imobile3.groovypayments.data.user.UserDataSource;
import com.imobile3.groovypayments.data.user.UserRepo;

import java.util.List;

public class UserViewModel extends ViewModel
{
    final private UserRepo REPO = UserRepo.getInstance(new UserDataSource());

    final private MutableLiveData<String> EMAIL = new MutableLiveData<>();

    public LiveData<UserEntity> getUserOnSearchQuery()
    {
        return Transformations.switchMap(EMAIL, new Function<String, LiveData<UserEntity>>()
        {
            @Override
            public LiveData<UserEntity> apply(String input)
            {
                return REPO.getUserByEmail(input);
            }
        });
    }

    public void SearchUserByEmail(String email)
    {
        this.EMAIL.setValue(email);
    }

    public void InsertUsers(UserEntity... userEntities)
    {
        REPO.InsertUsers(userEntities);
    }

    public LiveData<List<UserEntity>> GetAllUsers()
    {
        return REPO.getAllUser();
    }
}
