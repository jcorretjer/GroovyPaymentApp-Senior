package com.imobile3.groovypayments.utils.cryptography;

public interface Krypto
{
    public byte[] Encrypt(String text) throws Exception;

    public String Decrypt(byte[] encryptedText, byte[] iv) throws Exception;
}
