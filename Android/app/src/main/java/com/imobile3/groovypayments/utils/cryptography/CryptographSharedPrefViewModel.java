package com.imobile3.groovypayments.utils.cryptography;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.imobile3.groovypayments.data.enums.Tags;

public class CryptographSharedPrefViewModel extends AndroidViewModel
{
    private SharedPreferences sharedPreferences;

    private CryptographSharedPrefRepo repo;

    public CryptographSharedPrefViewModel(@NonNull Application application)
    {
        super(application);

        sharedPreferences = application.getSharedPreferences(Tags.SharedPref.SECURITY_PREF, Context.MODE_PRIVATE);

        repo = CryptographSharedPrefRepo.getInstance(sharedPreferences);
    }

    public void InsertEncodedKey(String key, String val)
    {
        repo.InsertEncodedKey(key, val);
    }

    public String GetEncodedKey(String key, String defaultVal)
    {
        return repo.GetEncodedKey(key, defaultVal);
    }
}
