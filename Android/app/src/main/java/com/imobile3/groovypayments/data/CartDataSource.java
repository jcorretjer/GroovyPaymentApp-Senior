package com.imobile3.groovypayments.data;

import com.imobile3.groovypayments.data.model.Cart;

import androidx.annotation.WorkerThread;
import androidx.lifecycle.LiveData;

import java.util.List;

public class CartDataSource {

    public CartDataSource() {
    }

    @WorkerThread
    public Result<List<Cart>> loadCarts() {
        List<Cart> results =
                DatabaseHelper.getInstance().getDatabase().getCartDao().getCarts();
        return new Result.Success<>(results);
    }

    public LiveData<List<Cart>> GetCarts()
    {
        return DatabaseHelper.getInstance().getDatabase().getCartDao().GetCarts();
    }
}
