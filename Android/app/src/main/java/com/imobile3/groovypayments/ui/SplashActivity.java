package com.imobile3.groovypayments.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.imobile3.groovypayments.MainApplication;
import com.imobile3.groovypayments.R;
import com.imobile3.groovypayments.data.enums.Tags;
import com.imobile3.groovypayments.ui.login.LoginActivity;
import com.imobile3.groovypayments.utils.cryptography.Cryptograph;
import com.imobile3.groovypayments.utils.cryptography.CryptographSharedPrefViewModel;
import com.imobile3.groovypayments.utils.test.unit.EspressoIdlingResource;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EspressoIdlingResource.increment();

        setContentView(R.layout.splash_activity);

        //region Cryptograph init

        /*
        * How this works is that `secureEncodedKey` contains the key currently stored on this device and `tempEncodedKey` contains a key generated using
        * the `secureEncodedKey`. If there was a key stored, then the temp and current should have the same value because the encryption algorithm will
        * generate the same key using the stored value. That way there's only one key per device. If not, generate a new key and store it on this device.
        */
        String secureEncodedKey,
                tempEncodedKey = "";

        final CryptographSharedPrefViewModel CRYPTOGRAPH_SHARED_PREF_VIEW_MODEL = ViewModelProviders.of(this).get(CryptographSharedPrefViewModel.class);

        secureEncodedKey = CRYPTOGRAPH_SHARED_PREF_VIEW_MODEL.GetEncodedKey(Tags.SharedPref.SECURE_ENCODED_KEY_KEY, "").replace(" ", "");

        try
        {
            Cryptograph.Init(secureEncodedKey);

            tempEncodedKey = Cryptograph.getInstance().GetEncodedSecretKey();
        }

        catch (InvalidKeySpecException | NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }

        if(tempEncodedKey.isEmpty())
            Toast.makeText(this, getResources().getString(R.string.secureEncodedKeyFailedToBuild), Toast.LENGTH_LONG)
                 .show();

        else if(!tempEncodedKey.equals(secureEncodedKey))
        {
            secureEncodedKey = tempEncodedKey;

            CRYPTOGRAPH_SHARED_PREF_VIEW_MODEL.InsertEncodedKey(Tags.SharedPref.SECURE_ENCODED_KEY_KEY, secureEncodedKey);
        }

        MainApplication.getInstance().setCryptographSharedPrefViewModel(CRYPTOGRAPH_SHARED_PREF_VIEW_MODEL);

        //endregion

        ImageView logo = findViewById(R.id.brand_logo);

        // Animate the brand logo to slide up from bottom.
        Animation animationUtils = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        logo.setAnimation(animationUtils);
        animationUtils.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                startLoginActivity();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    private void startLoginActivity() {
        new Handler().postDelayed(
                () -> {
                    SplashActivity.this.startActivity(
                            new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }, 500L);
    }

    @Override
    protected void initViewModel() {
        // Not used
    }
}
