package com.imobile3.groovypayments.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.imobile3.groovypayments.R;
import com.imobile3.groovypayments.data.TestDataRepository;
import com.imobile3.groovypayments.data.entities.UserEntity;
import com.imobile3.groovypayments.data.enums.Tags;
import com.imobile3.groovypayments.data.utils.User;
import com.imobile3.groovypayments.ui.BaseActivity;
import com.imobile3.groovypayments.ui.main.MainDashboardActivity;
import com.imobile3.groovypayments.utils.cryptography.Cryptograph;
import com.imobile3.groovypayments.utils.test.unit.EspressoIdlingResource;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import org.bouncycastle.util.encoders.Hex;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class LoginActivity extends BaseActivity {

    private LoginViewModel loginViewModel;

    private UserViewModel userViewModel;

    private UserEntity fakeUser = new UserEntity();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        EspressoIdlingResource.decrement();

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.btn_login);
        final Button btnSkipLogin = findViewById(R.id.btn_skip_login);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        loginViewModel.getLoginResult().observe(this, new Observer<LoginResult>() {
            @Override
            public void onChanged(@Nullable LoginResult loginResult) {
                if (loginResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                if (loginResult.getError() != null) {
                    showLoginFailed(loginResult.getError());
                }
                if (loginResult.getSuccess() != null) {
                    updateUiWithUser(loginResult.getSuccess());
                }
                handleLoginSuccess(new User().Build("", "", "").BuildEntity());
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginViewModel.login(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString());
                }
                return false;
            }
        });

        //TODO this fake user region must be removed once the user registration feature is implemented
        //region Insert fake user

        userViewModel.GetAllUsers().observe(this, new Observer<List<UserEntity>>()
        {
            @Override
            public void onChanged(List<UserEntity> userEntities)
            {
                if(userEntities.isEmpty() && !fakeUser.getEmail().isEmpty())
                    userViewModel.InsertUsers(fakeUser);
            }
        });

        try
        {
            fakeUser = TestDataRepository.getInstance().getUsers(TestDataRepository.Environment.GroovyDemo).get(0);
        }

        catch (InvalidKeySpecException |
                NoSuchAlgorithmException |
                IllegalBlockSizeException |
                InvalidKeyException |
                BadPaddingException |
                InvalidParameterSpecException |
                NoSuchPaddingException e)
        {
            e.printStackTrace();
        }

        //endregion

        //Passwd validation is done on this observer
        userViewModel.getUserOnSearchQuery().observe(this, new Observer<UserEntity>()
        {
            @Override
            public void onChanged(UserEntity userEntity)
            {
                String decryptedPasswd = "";

                if (userEntity != null)
                {
                    try
                    {
                        decryptedPasswd = Cryptograph.getInstance().Decrypt
                                (
                                        Hex.decode(userEntity.getPassword()),
                                        Hex.decode(userEntity.getPasswordIV())
                                );
                    }

                    catch (InvalidKeySpecException |
                            NoSuchAlgorithmException |
                            BadPaddingException |
                            InvalidKeyException |
                            InvalidAlgorithmParameterException |
                            NoSuchPaddingException |
                            IllegalBlockSizeException e)
                    {
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.passwordDecryptionFailed), Toast.LENGTH_LONG)
                             .show();
                    }

                    if (!decryptedPasswd.isEmpty() && passwordEditText.getText().toString().equals(decryptedPasswd))
                        handleLoginSuccess(userEntity);

                    else
                        Toast.makeText(getBaseContext(), getResources().getString(R.string.invalid_password2), Toast.LENGTH_LONG)
                             .show();
                }

                else
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.invalid_username), Toast.LENGTH_LONG)
                         .show();

                EspressoIdlingResource.decrement();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);

                EspressoIdlingResource.increment();

                userViewModel.SearchUserByEmail(usernameEditText.getText().toString());

                /*loginViewModel.login(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());*/
            }
        });

        btnSkipLogin.setOnClickListener(v -> handleLoginSuccess(new User().Build("", "", "").BuildEntity()));
    }

    @Override
    protected void initViewModel() {
        loginViewModel = ViewModelProviders.of(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);

        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
    }

    private void updateUiWithUser(LoggedInUserView model) {
        String welcome = getString(R.string.welcome) + model.getDisplayName();
        // TODO: Initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    private void handleLoginSuccess(UserEntity userEntity)
    {
        setResult(Activity.RESULT_OK);

        final Bundle BUNDLE = new Bundle();

        if(userEntity.getId() > 0)
            BUNDLE.putParcelable(Tags.Bundle.USER_PROFILE_KEY, userEntity);

        // Complete and destroy login activity once successful
        finish();
        startActivity(new Intent(LoginActivity.this, MainDashboardActivity.class).putExtras(BUNDLE));
    }
}
