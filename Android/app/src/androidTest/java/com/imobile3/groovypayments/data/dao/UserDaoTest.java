package com.imobile3.groovypayments.data.dao;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.imobile3.groovypayments.data.GroovyDatabase;
import com.imobile3.groovypayments.data.TestDataRepository;
import com.imobile3.groovypayments.data.entities.UserEntity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class UserDaoTest
{
    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    private GroovyDatabase mGroovyDatabase;

    @Before
    public void Init() {
        Context context = InstrumentationRegistry.getInstrumentation()
                                                 .getTargetContext();

        context.deleteDatabase(GroovyDatabase.NAME);

        mGroovyDatabase = Room.inMemoryDatabaseBuilder(context, GroovyDatabase.class)
                              .build();
    }

    @Test
    public void InsertTest() throws BadPaddingException,
            NoSuchPaddingException,
            NoSuchAlgorithmException,
            IllegalBlockSizeException,
            InvalidParameterSpecException,
            InvalidKeyException,
            InvalidKeySpecException
    {
        List<UserEntity> userEntities = InsertUsersByEnvironment(TestDataRepository.Environment.InstrumentationTest);

        List<UserEntity> dbUsers = mGroovyDatabase.getUserDao()
                                                  .getUsers();

        assertFalse(dbUsers.isEmpty());

        assertEquals(dbUsers.size(), userEntities.size());
    }

    @Test
    public void GetUserByEmailTest() throws BadPaddingException,
            InvalidKeySpecException,
            NoSuchAlgorithmException,
            NoSuchPaddingException,
            IllegalBlockSizeException,
            InvalidKeyException,
            InvalidParameterSpecException
    {
        MutableLiveData<String> email = new MutableLiveData<>();

        Transformations.switchMap(email, new Function<String, LiveData<UserEntity>>()
        {
            @Override
            public LiveData<UserEntity> apply(String input)
            {
                return mGroovyDatabase.getUserDao().getUserByEmail(input);
            }
        }).observeForever(new Observer<UserEntity>()
        {
            @Override
            public void onChanged(UserEntity userEntity)
            {
                assertEquals(TestDataRepository.DUMMY_EMAIL, userEntity.getEmail());
            }
        });

        email.setValue(InsertUsersByEnvironment(TestDataRepository.Environment.GroovyDemo).get(0).getEmail());
    }

    /**
     *
     * @return Inserted users
     */
    private List<UserEntity> InsertUsersByEnvironment(TestDataRepository.Environment environment) throws BadPaddingException, NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, InvalidParameterSpecException, InvalidKeyException, InvalidKeySpecException
    {
        List<UserEntity> userEntities = TestDataRepository.getInstance().getUsers(environment);

        mGroovyDatabase.getUserDao().insertUsers(userEntities.toArray(new UserEntity[0]));

        return userEntities;
    }
}
