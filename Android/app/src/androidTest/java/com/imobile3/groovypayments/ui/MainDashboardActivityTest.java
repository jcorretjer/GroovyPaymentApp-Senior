package com.imobile3.groovypayments.ui;

import androidx.test.espresso.IdlingPolicies;
import androidx.test.espresso.IdlingRegistry;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;

import com.imobile3.groovypayments.R;
import com.imobile3.groovypayments.data.TestDataRepository;
import com.imobile3.groovypayments.ui.adapter.MainDashboardButton;
import com.imobile3.groovypayments.ui.main.MainDashboardActivity;
import com.imobile3.groovypayments.utils.test.unit.EspressoIdlingResource;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class MainDashboardActivityTest
{
    @Rule
    public ActivityScenarioRule<SplashActivity> activityScenarioRule = new ActivityScenarioRule<>(SplashActivity.class);

    private int userProfileIndex;

    @Before
    public void Init()
    {
        userProfileIndex = MainDashboardActivity.getDashboardBtns().indexOf(MainDashboardButton.UserProfile);

        IdlingRegistry.getInstance().register(EspressoIdlingResource.instance);

        IdlingPolicies.setMasterPolicyTimeout(EspressoIdlingResource.WAIT_TIME * EspressoIdlingResource.WAIT_TIME, TimeUnit.MILLISECONDS);

        IdlingPolicies.setIdlingResourceTimeout(EspressoIdlingResource.WAIT_TIME * EspressoIdlingResource.WAIT_TIME, TimeUnit.MILLISECONDS);
    }

    @After
    public void Fin()
    {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.instance);
    }

    @Test
    public void SkipLoginAndClickOnUserProfileTest()
    {
        SkipLogin();

        ClickUserProfileAndValidateVisibility();
    }

    @Test
    public void LoginAndClickOnUserProfileTest()
    {
        Login();

        ClickUserProfileAndValidateVisibility();
    }

    private void ClickUserProfileAndValidateVisibility()
    {
        onView(withId(R.id.grid_launch_buttons)).perform(RecyclerViewActions.actionOnItemAtPosition(userProfileIndex, click()));

        onView(withId(R.id.icon)).check(matches(isDisplayed()));
    }

    private void SkipLogin()
    {
        onView(withId(R.id.btn_skip_login)).perform(click());

        onView(withId(R.id.grid_launch_buttons)).check(matches(isDisplayed()));
    }

    private void Login()
    {
        onView(withId(R.id.username)).perform(replaceText(TestDataRepository.DUMMY_EMAIL));

        onView(withId(R.id.password)).perform(replaceText(TestDataRepository.DUMMY_PASSWD));

        onView(withId(R.id.btn_login)).perform(click());

        onView(withId(R.id.grid_launch_buttons)).check(matches(isDisplayed()));
    }
}
