package com.imobile3.groovypayments.utils.test.unit;

import android.text.format.DateUtils;

import androidx.test.espresso.IdlingResource;
import androidx.test.espresso.idling.CountingIdlingResource;

public class EspressoIdlingResource implements IdlingResource
{
    private static final String RESOURCE = "GLOBAL";

    public static final long WAIT_TIME = DateUtils.MINUTE_IN_MILLIS * 2;

    private static long startTime;

    private ResourceCallback resourceCallback;

    public static CountingIdlingResource instance = new CountingIdlingResource(RESOURCE);

    public static void increment()
    {
        instance.increment();

        startTime = System.currentTimeMillis();
    }

    public static void decrement()
    {
        if(!instance.isIdleNow())
            instance.decrement();
    }

    @Override
    public String getName()
    {
        return EspressoIdlingResource.class.getName() + ":" + WAIT_TIME;
    }

    @Override
    public boolean isIdleNow()
    {
        long elapsed = System.currentTimeMillis() - startTime;

        boolean idle = elapsed >= WAIT_TIME;

        if(idle)
            resourceCallback.onTransitionToIdle();

        return idle;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback callback)
    {
        this.resourceCallback = resourceCallback;
    }
}
